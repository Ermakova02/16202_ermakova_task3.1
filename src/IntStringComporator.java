import java.util.Comparator;

public class IntStringComporator implements Comparator<IntStringPair> {
    @Override
    public int compare(IntStringPair o1, IntStringPair o2) {
        if (o2.GetNumber() > o1.GetNumber()) return 1; // Сортировка в обратном порядке!
        if (o2.GetNumber() < o1.GetNumber()) return -1; // Сортировка в обратном порядке!
        return o1.GetStr().compareTo(o2.GetStr());
    }
}
