public class IntStringPair
{
    private String mStr;
    private Integer mNumber;
    IntStringPair(String s, Integer n)
    {
        mStr = s;
        mNumber = n;
    }
    public String GetStr() {
        return mStr;
    }
    public Integer GetNumber() {
        return mNumber;
    }
}
///////