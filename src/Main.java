import java.io.*;
import java.lang.*;
import java.util.*;
//import java.util.HashSet*;

public class Main {
    public static void main(String[] args) {
        WordsStatistic stat = new WordsStatistic();
        String fileName = args[0];
        StringBuilder currWord = new StringBuilder("");
        Reader reader = null;
        try {
            reader = new InputStreamReader(new FileInputStream(fileName), "CP1251");
            int c = 0;
            while ((c = reader.read()) != -1) {
                if (Character.isLetterOrDigit((char) c))
                    currWord.append((char) c);
                else if (currWord.length() > 0) {
//                    System.out.println("AddWord: " + currWord);
                    stat.AddWord(currWord.toString());
                    currWord.delete(0, currWord.length());
                }
            }
            if (currWord.length() > 0) {
//                System.out.println("AddWord: " + currWord);
                stat.AddWord(currWord.toString());
                currWord.delete(0, currWord.length());
            }
            stat.PrepareStatistic();
        }
        catch (IOException e) {
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }
        finally {
            if (reader != null) {
                try {
                    reader.close();
                }
                catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            }
        }
        /////////////////////////////////////////////////////////////////////
        Writer writer = null;
        try {
            writer = new OutputStreamWriter(new FileOutputStream(fileName + ".csv", false), "CP1251");
            stat.MoveFirstEntry();
            while(stat.HasNextEntry())
            {
                stat.NextEntry();
//                System.out.println("WriteLine: " + stat.GetLine());
                writer.write(stat.GetLine());
            }
        }
        catch (IOException e) {
            System.err.println("Error while writing file: " + e.getLocalizedMessage());
        }
        finally {
            if (writer != null) {
                try {
                    writer.close();
                }
                catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            }
        }
    }
}
