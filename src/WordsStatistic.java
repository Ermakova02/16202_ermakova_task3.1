import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

public class WordsStatistic {
    private HashMap<String, Integer> mWordsMap;
    private TreeSet<IntStringPair> mWordsSet;
    private Iterator<IntStringPair> mEntries;
    private IntStringPair mEntry;
    private IntStringComporator mComp;
    private float mTotalFreq;
    WordsStatistic() {
        mComp = new IntStringComporator();
        mWordsMap = new HashMap<String, Integer>();
        mWordsSet = new TreeSet<IntStringPair>(mComp);
        mEntries = mWordsSet.iterator();
        mEntry = new IntStringPair("", 0);
        mTotalFreq = (float)0.0;
    }
    public void AddWord(String s) {
        Integer freq = 1;
        if (mWordsMap.containsKey(s)) {
            freq = mWordsMap.get(s);
            freq++;
           }
        mWordsMap.put(s, freq);

    }
    public void Clear() {
        mWordsMap.clear();
    }
    public void PrepareStatistic(){
        Iterator<HashMap.Entry<String, Integer>> mapEntries = mWordsMap.entrySet().iterator();
        mTotalFreq = (float)0.0;
        while (mapEntries.hasNext()) {
            HashMap.Entry<String, Integer> entry = mapEntries.next();
            IntStringPair pair = new IntStringPair(entry.getKey(), entry.getValue());
            mTotalFreq += (float)entry.getValue();
            mWordsSet.add(pair);
        }
    }
    public void MoveFirstEntry(){
        mEntries = mWordsSet.iterator();
    }
    public boolean HasNextEntry() {
        return mEntries.hasNext();
    }
    public void NextEntry() {
        mEntry = mEntries.next();
    }
    public String GetLine() {
        float percentFreq = (float)mEntry.GetNumber() / mTotalFreq * (float) 100.0;
        return mEntry.GetStr() + "," + mEntry.GetNumber()+ "," + percentFreq + "\n";
    }
};

